#!/usr/bin/python
#
# Copyright (c) 2018 Peter Palfrader
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

def upgrade(db):
    """We now have files in the archive with huge names, breaking our
       previous limits.  e.g.
         node-debbundle-acorn_6.2.1+ds+~0.4.0+~4.0.0+really4.0.0+~1.0.0+~5.0.1+ds+~1.7.0+ds+~0.1.1+~0.3.1+~0.2.0+~0.1.0+~0.3.0+~0.3.0-2_all.deb

       So replace all varchars with TEXT
    """

    #
    # alter tables
    #
    db.execute("""
        ALTER TABLE archive ALTER COLUMN name TYPE TEXT;
        """)

    db.execute("""
        ALTER TABLE directory ALTER COLUMN path TYPE TEXT;
        """)
    db.execute("""
        ALTER TABLE symlink ALTER COLUMN name TYPE TEXT,
                            ALTER COLUMN target TYPE TEXT;
        """)
    db.execute("""
        ALTER TABLE file ALTER COLUMN name TYPE TEXT;
        """)

    db.execute("""
        ALTER TABLE binpkg ALTER COLUMN name TYPE TEXT;
        """)
    db.execute("""
        ALTER TABLE srcpkg ALTER COLUMN name TYPE TEXT;
        """)

    db.execute("""
        ALTER TABLE file_binpkg_mapping ALTER COLUMN architecture TYPE TEXT;
        """)
    db.execute("""
        ALTER TABLE indexed_mirrorrun ALTER COLUMN source TYPE TEXT;
        """)


    db.execute("""
        DROP VIEW node_with_ts;
        """)
    db.execute("""
        ALTER TABLE mirrorrun ALTER COLUMN importing_host TYPE TEXT;
        """)
    # recreate a it was before
    db.execute("""
        CREATE VIEW node_with_ts AS
            SELECT node.*,
                   first_run.run AS first_run,
                   first_run.archive_id,
                   last_run.run AS last_run
            FROM node,
                 (SELECT * FROM mirrorrun) AS first_run,
                 (SELECT * FROM mirrorrun) AS last_run
            WHERE first_run.mirrorrun_id=node.first
              AND last_run.mirrorrun_id=node.last;
        """)
    db.execute("""
        GRANT SELECT ON node_with_ts TO public;
        """)



    #
    # alter result types
    #
    db.execute("""
        ALTER TYPE dirtree_result ALTER ATTRIBUTE path TYPE TEXT,
                                  ALTER ATTRIBUTE name TYPE TEXT,
                                  ALTER ATTRIBUTE target TYPE TEXT;
        """)
    db.execute("""
        ALTER TYPE readdir_result ALTER ATTRIBUTE name TYPE TEXT,
                                  ALTER ATTRIBUTE target TYPE TEXT;
        """)
    db.execute("""
        ALTER TYPE stat_result ALTER ATTRIBUTE path TYPE TEXT;
        """)


    #
    # recreate functions with TEXT instead of VARCHAR(nn)
    #
    db.execute("""
        CREATE OR REPLACE FUNCTION dirtree(in_mirrorrun_id integer) RETURNS SETOF dirtree_result AS $$
        DECLARE
            mirrorrun_run timestamp;
            arc_id integer;
        BEGIN
            SELECT run, archive_id INTO mirrorrun_run, arc_id FROM mirrorrun WHERE mirrorrun_id = in_mirrorrun_id;
            RETURN QUERY
                WITH RECURSIVE

                subdirs(first, path, directory_id) AS
                ( SELECT node_with_ts.first, path, directory_id
                    FROM directory NATURAL JOIN node_with_ts
                    WHERE path='/'
                      AND first_run <= mirrorrun_run
                      AND last_run  >= mirrorrun_run
                      AND archive_id = arc_id
                UNION ALL
                  SELECT node_with_ts2.first, directory.path, directory.directory_id
                    FROM directory NATURAL JOIN node_with_ts2
                    JOIN subdirs ON node_with_ts2.parent = subdirs.directory_id
                    WHERE node_with_ts2.parent <> directory.directory_id
                      AND first_run <= mirrorrun_run
                      AND last_run  >= mirrorrun_run
                )

                SELECT first, NULL AS size, 'd'::CHAR AS filetype, path, NULL::TEXT AS name, NULL::CHAR(40) AS hash, NULL::TEXT AS target
                    FROM subdirs
                UNION ALL
                  SELECT node_with_ts2.first, size, '-'::CHAR, path, name, hash, NULL::TEXT AS target
                     FROM file NATURAL JOIN node_with_ts2
                     JOIN subdirs ON subdirs.directory_id = node_with_ts2.parent
                   WHERE first_run <= mirrorrun_run
                     AND last_run  >= mirrorrun_run
                UNION ALL
                  SELECT node_with_ts2.first, NULL as size, 'l'::CHAR, path, name, NULL::CHAR(40) AS hash, target
                     FROM symlink NATURAL JOIN node_with_ts2
                     JOIN subdirs ON subdirs.directory_id = node_with_ts2.parent
                   WHERE first_run <= mirrorrun_run
                     AND last_run  >= mirrorrun_run
                ;
        END
        $$ LANGUAGE plpgsql;
        """)

    db.execute("""
        DROP FUNCTION readdir(in_directory VARCHAR, in_mirrorrun_id integer);
        """)
    db.execute("""
        CREATE OR REPLACE FUNCTION readdir(in_directory TEXT, in_mirrorrun_id integer) RETURNS SETOF readdir_result AS $$
        DECLARE
            mirrorrun_run timestamp;
            dir_id integer;
            arc_id integer;
        BEGIN
            SELECT run, archive_id INTO mirrorrun_run, arc_id FROM mirrorrun WHERE mirrorrun_id = in_mirrorrun_id;
            SELECT directory_id INTO dir_id
               FROM directory JOIN node_with_ts ON directory.node_id = node_with_ts.node_id
               WHERE path=in_directory
                 AND node_with_ts.archive_id = arc_id
                 AND first_run <= mirrorrun_run
                 AND last_run  >= mirrorrun_run;
            RETURN QUERY
                SELECT 'd'::CHAR, substring(path, '[^/]*$')::TEXT, node_with_ts.node_id, NULL::CHAR(40), NULL, NULL::TEXT AS target
                  FROM directory NATURAL JOIN node_with_ts
                  WHERE parent=dir_id
                    AND directory_id <> parent
                    AND first_run <= mirrorrun_run
                    AND last_run  >= mirrorrun_run
                UNION ALL
                SELECT '-'::CHAR, name, node_with_ts.node_id, file.hash, file.size, NULL::TEXT
                  FROM file NATURAL JOIN node_with_ts
                  WHERE parent=dir_id
                    AND first_run <= mirrorrun_run
                    AND last_run  >= mirrorrun_run
                UNION ALL
                SELECT 'l'::CHAR, name, node_with_ts.node_id, NULL::CHAR(40), NULL, symlink.target
                  FROM symlink NATURAL JOIN node_with_ts
                  WHERE parent=dir_id
                    AND first_run <= mirrorrun_run
                    AND last_run  >= mirrorrun_run;
        END;
        $$ LANGUAGE plpgsql;
        """)

    db.execute("UPDATE config SET value='22' WHERE name='db_revision' AND value='21'")

# vim:set et:
# vim:set ts=4:
# vim:set shiftwidth=4:
